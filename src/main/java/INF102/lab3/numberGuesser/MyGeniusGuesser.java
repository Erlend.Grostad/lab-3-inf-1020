package INF102.lab3.numberGuesser;



public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int low = number.getLowerbound();
        int high = number.getUpperbound();
        while (true) {
            int mid = low + (high - low) / 2;
            int guess = number.guess(mid);
            if (guess == 0)
                return mid;
            else if (guess < 0)
                low = mid;
            else
                high = mid;
            }   
    }

}
