package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    public long sum(List<Long> list) {
        
        if (list.size() == 0) {
            return 0;
        }
        
        return list.remove(list.size() - 1) + sum(list);
    }
    

}
